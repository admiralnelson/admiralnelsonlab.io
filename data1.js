const
    DATA =
    {
        title: `Krisnhadi Bima`,
        shortPararaph: ` `,
        links:
            [
                {
                    icon: "far fa-envelope-open",
                    url: "mailto:krisnadibima98@gmail.com",
                    text: "krisnadibima98@gmail.com"
                },
                {
                    icon: "fab fa-github",
                    url: "https://github.com/admiralnelson",
                    text: "admiralnelson"
                },
                {
                    icon: "fab fa-linkedin",
                    url: "https://www.linkedin.com/in/krisnhadi-bima-68235214b/",
                    text: "Krisnadibima"
                },
                {
                    icon: "fab fa-internet-explorer",
                    url: "https://admiralnelson.gitlab.io/",
                    text: "admiralnelson.gitlab.io"
                }
            ],
        sections:
            [
                {
                    url: "icons/1.png",
                    text: "Technical Skills",
                    body: [
                        [
                            "Programming Languages & Frameworks",
                            `C#/VB.net (ASP.Net Core), C++, Python, ESP-IDF, Arduino, JS (Vue.js)`,

                        ],
                        [
                            "Database",
                            "MariaDB/MySQL, SQLite",
                        ],
                        [
                            "Others",
                            "Unity, Inkscape, LibreOffice, IIS, Apache 2.0, Git, SSH",
                        ],
                        [
                            "Languange",
                            "English, Bahasa Indonesia"
                        ]
                    ]
                },
                {
                    url: "icons/2.png",
                    text: "Projects",
                    body: [
                        [
                            "2022",
                            `Knights of Round Belly`,
                            "Lua, Asset Editor, Blender",
							"https://steamcommunity.com/sharedfiles/filedetails/?id=2790318113"
                        ],
                        [
                            "2022",
                            `libsneedio, a library that provides custom music and audio`,
                            "C++, Lua",
							"https://github.com/admiralnelson/warhammer2-libsneedio"
                        ],
                        [
                            "2020",
                            `Exception/Error Debugger Window for Mount and Blade: Bannerlord`,
                            "C#",
							"https://www.nexusmods.com/mountandblade2bannerlord/mods/404"
                        ],
                        [
                            "2019",
                            `Survey Boat Drone project (X-Boat)`,
                            "Python",
							"https://drive.google.com/file/d/1Uc4ymx-lKsDKEaxLQL6coH_ZL5WmbWVz/view?usp=sharing"
                        ],
                        [
                            "2019",
                            `Sirmoto, automatic gardening system and service for Republic of IoT Workshop and competition`,
                            "ESP-IDF SDK, ESP32, C++, ASP.Net (backend)",
                            "https://drive.google.com/drive/folders/1oNyv6qn2HnhJDeJokrBCjsDo4SwGp4jc?usp=sharing"
                        ],
                        [
                            "2019",
                            `simply-valore.com, A hotel booking service and homepage for Simply Valore Hotel at Cimahi. `,
                            "PHP Codeigniter 3, MySQL, Vue.js",
							"http://simply-valore.com"
                        ]
                    ]
                },
                {
                    url: "icons/3.png",
                    text: "Achievements",
                    body: [
                        [
                            "2019",
                            `2nd in Gameloft Student Competition: Instanced Geometry to Optimse OpenGL ES 3 Performance `,
                            " "
                        ],
                        [
                            "2018",
                            `Finalist Republic of IoT`,
                            " "
                        ],
                        [
                            "2018",
                            `5th runner up in "Festival Bisnis Widyatama"`,
                            " "
                        ],
                        [
                            "2015",
                            `1st in "Karya Cipta Elektronik Politeknik Negeri Bali: Smart Socket"`,
                            " "
                        ],
                    ]
                },
                {
                    url: "icons/4.png",
                    text: "Experience",
                    body: [
                        [
                            "",
                            `Fulltime at Gameloft Studio Yogyakarta as Junior C++ Programmer`,
                            "2020-2021"

                        ],
                        [
                            "",
                            `Internship at Gameloft Studio Yogyakarta as Game Programmer`,
                            "2019 (3 months)"

                        ],
                        [
                            "",
                            `Freelancing`,
                            "2017-2019"
                        ],
                        [
                            "",
                            `Teacher assistant at Telkom Univeristy on OOP & Java subject`,
                            "2018"
                        ],
                    ]
                },
                {
                    url: "icons/5.png",
                    text: "Education",
                    body: [
                        [
                            "",
                            `Telkom University`,
                            "2016-2020"

                        ],
                        [
                            "",
                            `SMAN 2 High School Denpasar`,
                            "2013-2016"
                        ],

                    ]
                },
                {
                    url: "icons/6.png",
                    text: "Publication",
                    body: [
                        [
                            "Kinetik",
                            `A Performance Analysis of General Packet Radio Service (GPRS) and Narrowband Internet of Things (NB-IoT) in Indonesia`,
                            "2020",
							"http://kinetik.umm.ac.id/index.php/kinetik/article/view/947",
                        ],
						[
                            "IOP",
                            `Narrowband-IoT network for asset tracking system`,
                            "2020",
							"https://iopscience.iop.org/article/10.1088/1757-899X/830/2/022087",
                        ],
                    ]
                }
            ],
		blogs: [
			{
				title: "X-Boat",
				url: "url",
				picture: "https://lh3.googleusercontent.com/HGZqqDt1WdKhvFBsco6zY6-UWtT2JAqiIyctra9XDTGtgZYKl9vqRGpGZR3N0lEuty_fWHNIXojd1hmXrSKKsXCd7UaFmuXFyyB9PrNAQGCrMNM3PucPrtr6aGqIeEcrrAgKEqdUryNd06WnFCdl1Fsil4rUAtAe1ifKBmH9GIV-o-0J7ZjakA6X9iUazWTVTlJu8RMmCDSQeOuuO1Bn3_HG4R758WHyzk1JpQHLyt8FORHKTAdwgeYCysH9s-wLFXD2dzaA2NcXPcEErt7gNVtilTFN9TJih77in5F185p9Zlkneexs5kp0_YF-sShVsR5LeZMOulfDsOUc64RP0Qn-lBtSXyErp20ksDV75zQWMUqvZ54dOcVPPvUVMVjQ_lQC2NtfRGkja2Nn3_QZjn2ZS3AvU9zL0op42kG9NfkEX3t_lbYuetbTtPmbfC1Th4nN9y88ApVkHe66TGryz1_ztsZKeQFIIx60-bSHcSAQ9cdGry2_gN7HfRuJP0QH91B0u-G8XM37CrKqIfA4xIcjAuuOpBO7Q5fPBppN1zYZYpITKQR51q6AFf43QPLePzO6KI295ULN8b3_2bX3wg7_zHq8xhuK8sfHEq9ZaCyG-uaGlYa0t2fUt-bJqWtufEpiSe0sqXNely38jAXcB_7eR7Bmm8Bq-kzcTW4zdhnijGKSfyCLpWc=w1560-h878-no?authuser=0",
				resume: "Me and my friends created autonomus drone. It's based on Pixhawk module, a raspberry Pi running python script, and  Echologger Sonar sensor."
			}
		]
    }
