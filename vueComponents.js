Vue.component(
    "title-section",
    {
        props: ["paragraph"],
        template: `
        <div>
                <p><span class="caps">{{paragraph[0] }}</span>
                {{paragraph.slice(1, paragraph.length)}}</p>

        </div>                    
            `
    }
);

Vue.component(
    "sub-header",
    {
        props: ["title", "links" , "paragraph"],
        template:
        `
        <div style="margin-bottom:5px">
            <div class="mainTitle">{{title}}</div>
            <div>
                <ul>
                    <li v-for="link in links">
                        <a v-if="link.url" v-bind:href="link.url">
                            <i v-if="link.icon" v-bind:class="link.icon"></i>
                            {{link.text}}
                        </a>
                        <span v-else="" class="text">
                            <i v-if="link.icon" v-bind:class="link.icon"></i>
                            {{link.text}}
                        </span>
                    </li>
                </ul>    
            </div>
            <div>{{paragraph}}</div>
            <hr></hr>
        </div>
        
        `
    }
);

Vue.component(
    "list",
    {
        props:["rows"],
        template:`
        <div>
            <table style="width:100%">
                <tr v-for="row in rows">
                    <td v-if="row[0]" class="leftCol">
						{{row[0]}}
                    </td>
                    <td v-if="row[1] && row[2] == ''" class="fullcentreCol">
                        <template v-if="row[3]"> 
							<a v-on:click="navigate(row[3])" v-bind:href="row[3]"> {{row[1]}} </a>
						</template>
						<template v-else>
							{{row[1]}}
						</template>
                    </td>
                    <td v-else-if="row[1]" class="centreCol">
                        <template v-if="row[3]"> 
							<a v-bind:href="row[3]" v-on:click="navigate(row[3])"> {{row[1]}} </a>
						</template>
						<template v-else>
							{{row[1]}}
						</template>
                    </td>
                    <td v-if="row[2] != undefined" class="rightCol">
                        {{row[2]}}
                    </td>
                </tr>
            </table>
        </div>
        `,
		methods: {
			navigate: function(url)
			{
				//if(!url.startsWith("navigate"))
				//{
				//	window.open(url,'_blank');
				//}
				//else
				//{
				//	//if(window.innerWidth <= 767) return;
				//	g_Main.article = url.substr(9);
				//	g_Main.page = g_Main.PAGE.ARTICLE;
				//}
			}
			
		}
    }
)

Vue.component(
    "sub-paragraph",
    {
        props: ["object", "body"],
        template: `
        <div>
            <div>
                <span><img :src="object.url" class="icon"></img></span>
                <span class="title">{{object.text}}</span>
            </div>
            <div v-if="object.body">
                <list v-bind:rows="object.body">
                </list>
            </div>
            <div v-else>
                Fug
            </div>
            
        </div>
        `
    }
);

Vue.component("blog-resume", 
	{
		props: ["title", "url" ,"picture"],
		template: `
		<div style="min-height:130px;">
			<p style="float: left;margin-right:10px">
				<template v-if="picture">
					<img v-bind:href="picture"></img>
				</template>
				<template v-else>
					<img v-bind:src="picture" height="100px" width="100px" border="1px">
				</template>
			</p>
			<a href="#" v-on:click="navigate(url)">
				<h2>
					{{title}}
				</h2>
			</a>
			<p>
				<slot></slot>
			</p>
		</div>
		`,
		methods: {
			navigate: function(url)
			{
				alert("open internal page " + url);
			}
			
		}
	}
);

Vue.component("blog-page", 
	{
		props: ["url"],
		data: function () { return { page: "" } },
		created: function()
		{
			var xhttp = new XMLHttpRequest();
			var component = this;
			xhttp.onreadystatechange = function() 
			{
				if (this.readyState == 4 && this.status == 200) 
				{
					component.page = this.responseText;
				}
			};
			xhttp.open("GET", component.url, true);
			xhttp.send();
		},
		template: `
		<div v-html="page"></div>
		`
	}
)
	