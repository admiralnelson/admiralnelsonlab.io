const PAGE_CV = 0
const PAGE_BLOG = 1

var g_Main = new Vue(
    {
        el: '#Main',
        data: {
			data: DATA,
			page: PAGE_CV,
			article: ""
		},
		created: function() {
			this.PAGE = { CV: 0, BLOG: 1, ARTICLE: 2 };
		}
    });
    